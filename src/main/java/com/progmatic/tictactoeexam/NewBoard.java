/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author NoDi
 */
public class NewBoard implements Board {

    private final Cell[][] gameboard = new Cell[3][3];

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {

        if (rowIdx < 3 && colIdx < 3 && rowIdx >= 0 && colIdx >= 0) {
            if (gameboard[rowIdx][colIdx].getCellsPlayer() == PlayerType.O) {
                return PlayerType.O;
            } else if (gameboard[rowIdx][colIdx].getCellsPlayer() == PlayerType.X) {
                return PlayerType.X;
            } else {
                return PlayerType.EMPTY;
            }
        } else {
            throw new CellException(rowIdx, colIdx, "The  number is to big.");
        }
    }

    @Override
    public void put(Cell cell) throws CellException {

        if (gameboard[cell.getRow()][cell.getCol()].getCellsPlayer() != PlayerType.EMPTY) {
            gameboard[cell.getRow()][cell.getCol()] = cell;
        } else {
            throw new CellException(cell.getRow(), cell.getCol(), "The cell is not empty.");
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {

        int firstplayerdiagonalcnt = 0;
        int secondplayerdiagonalcnt = 0;
        int firstplayerlinecnt = 0;
        int secondplayerlinecnt = 0;
        int column = 0;
        int column2 = 0;
        for (int i = 0; i <= gameboard.length - 1; i++) {
            for (int j = 0; j <= gameboard.length - 1; j++) {
                if (j == i) {
                    p.O.equals(gameboard[i][j]);
                    firstplayerdiagonalcnt++;
                } else if (firstplayerdiagonalcnt == 3) {
                    return true;
                }
                if (j + i == gameboard.length - 1) {
                    p.X.equals(gameboard[i][j]);
                    secondplayerdiagonalcnt++;
                } else if (secondplayerdiagonalcnt == 3) {
                    return true;
                }
            }
        }
        for (int i = 0; i <= gameboard.length - 1; i++) {
            for (int j = 0; j <= gameboard.length - 1; j++) {
                if (p.O.equals(gameboard[i][j])) {
                    firstplayerlinecnt++;
                } else if (firstplayerlinecnt == 3) {
                    return true;
                }
                if (p.X.equals(gameboard[i][j])) {
                    secondplayerlinecnt++;
                } else if (secondplayerlinecnt == 3) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> empty = new ArrayList<>();
        Cell cell = new Cell(0, 0);
        for (Cell[] cells : gameboard) {
            if (cells.equals(PlayerType.EMPTY)) {
                empty.add(cell);
            }
        }
        return empty;
    }

}
